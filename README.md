# gulp-primer

- Sass compilation and prefixing
- JavaScript module bundling with webpack
- Built-in BrowserSync server
- For production builds:
  - CSS compression
  - JavaScript compression
  - Image compression

## Prerequisites

Make sure you have the following installed:

* [Yarn](https://yarnpkg.com/en/docs/install)

### Setup

To set up the template, first download it.

Clone repo into your existing project and install dependencies:

```bash
git clone git@gitlab.com:ellucianwebservices/gulp-primer.git .
yarn
```

Finally, run `yarn start` to run Gulp. Your finished site will be created in a folder called `public`, viewable at this URL:

```
http://localhost:8000
```
